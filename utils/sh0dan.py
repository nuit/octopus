# -*- coding: utf-8 -*-
import shodan
SHODAN_API_KEY = "T0p.Secret.K"
api = shodan.Shodan(SHODAN_API_KEY)

#colors
R='\033[91m'
G='\033[92m'
B='\033[94m'
ENDC='\033[0m'

def heian_shodan(ip):
        global d_shodan
        d_shodan=dict()
        l_ports=[]
        l_hosts=[]
        host = api.host(ip)
        print '>>> host:',host
        d_shodan['org']=host.get('org', 'n/a')
        t_os=host.get('os', 'n/a')
        if t_os != None:
                d_shodan['os']=t_os

        for item in host['data']:
                print '>>> item:',item
                t_port=item['port']
                l_ports.append(t_port)
                t_transport=item['transport']
                l_ports.append(t_transport)
                t_hostnames=item['hostnames']
                if '.' not in t_hostnames:
                        l_hosts.append(t_hostnames)

                try:
                    # product                   
                        d_shodan['prod_version']=item['version']
                        d_shodan['prod_prod']=item['product']
                        d_shodan['prod_hash']=item['hash']
                        d_shodan['prod_timestamp']=item['timestamp']
                        d_shodan['prod_link']=item['link']
                        # location
                        d_shodan['city']=item['location']['city']
                        d_shodan['region_code']=item['location']['region_code']
                        d_shodan['area_code']=item['location']['area_code']
                        d_shodan['longitude']=item['location']['longitude']
                        d_shodan['country_code3']=item['location']['country_code3']
                        d_shodan['country_name']=item['location']['country_name']
                        d_shodan['postal_code']=item['location']['postal_code']
                        d_shodan['dma_code']=item['location']['dma_code']
                        d_shodan['country_code']=item['location']['country_code']
                        d_shodan['latitude']=item['location']['latitude']
                except:
                        print R+'\n[SHODAN_FAIL]'+ENDC,'Extracting data x_X'
                        pass

                try:
                        # ssh
                        t_ssh_fingerprint=item['ssh']['fingerprint']
                        d_shodan['ssh_fingerprint']=t_ssh_fingerprint
                        t_ssh_mac=item['ssh']['mac']
                        d_shodan['ssh_mac']=t_ssh_mac
                        t_ssh_cipher=item['ssh']['cipher']
                        d_shodan['ssh_cipher']=t_ssh_cipher
                        #t_ssh_key=item['ssh']['key']
                        #d_shodan['ssh_key']=str(t_ssh_key)
                except:
                        print R+'\n[SHODAN_FAIL]'+ENDC,'No ssh data :/'
                        pass

                if t_transport=='tcp':
                    t_banner=item['data'].replace('-','').replace('\n','-').replace('\t',' ').split()
                if ':' not in t_banner:
                                d_shodan['tcp_banner']=' '.join(t_banner)

# ports: tcp: 443: tcp: 22: tcp: 80
        d_shodan['ports']=''.join(str(l_ports[::-1])).strip('[]').replace("'","").replace("u","").replace(",","")
        d_shodan['hostnames']=''.join(str(l_hosts)).strip("[[]]").replace("u","").replace("'","").replace("[","").replace("]","")
        return d_shodan

def search_shodan(target):
        sdata=list()
        i=0
        try:
                results = api.search(target)
                print 'Results found: %s' % results['total']
                for result in results['matches']:
                        sdata.append(result['ip_str'])
                        data=str(result['data']).replace('\r\n',' - ')
                        print R+'[search_shodan] data:'+ENDC,data
                        sdata.append(data)
                return sdata
        except shodan.APIError, e:
                msg_erro='Error:',e
                return msg_erro
