# -*- coding: utf-8 -*-
from socket import socket
import re, time
import urllib, urllib2
import httplib2
from bs4 import BeautifulSoup
import mechanize
import requests
import cookielib

#colors
R='\033[91m'
G='\033[92m'
B='\033[94m'
ENDC='\033[0m'

### networking
def find_email(data):
    regexStr = r'^([^@]+)@[^@]+$'
    matchobj = re.search(regexStr, data)
    if not matchobj is None:
        email=matchobj.group(0)
        host_email=email.split('@')[1]
        print host_email
    else:
        print "Did not match"

def get_ip(url):
    import socket
    addr = socket.gethostbyname(url)
    return addr

# remove 'http' for the nasty sockets 
def clean_http(url):
    url_http = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', url)
    if url_http:
        url_http=url_http.split('/')[2]
        return url_http
    else:
        pass

# search headers
def header_url(url):
    url_http = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', url)
    if url_http:
        print '[URL_HTTP]',url_http
    else:
        url_http='http://'+url

    http_interface = httplib2.Http(".cache", disable_ssl_certificate_validation=True)

    try:
        resp= http_interface.request(url_http, method="HEAD")[0]
        print '--- resp ---'
        print resp # dict with all headers
        return resp

    except httplib2.ServerNotFoundError, e:
        return str(e.message)
        print '[-] Error:',e.message

def wh0is(ip):
    ip=''.join(ip)
    url="http://www.ip-tracker.org/locator/ip-lookup.php?ip="+str(ip)
    br = mechanize.Browser()
    br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
    cj = cookielib.LWPCookieJar()
    br.set_cookiejar(cj)
    br.set_handle_equiv(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)
    s=br.open(url)
    html=s.read()
    soup=BeautifulSoup(html, 'html5lib')
    res=[]

    for td in soup.findAll('table',)[2].findAll('td'):
        td=td.text
        td=td.replace('td:','').replace('[IP Blacklist Check]','').replace('>>','').encode('utf-8')
        td=td.replace('\n','').replace('\t','')
        res.append(td)
    d={'Target':res[0],'Country':res[3],'IDD Code':res[-1],'Currency':res[-2],'Language':res[-4],
    'City Lat/Lon':res[-5],'Country Lat/Lon':res[-6],'Continent Lat/Lon':res[-7],'Sunrise / Sunset':res[-8],
    'Timezone GMT offset':res[-9],'Local Time':res[-10],'Time Zone':res[-11]}

    info=[d['Country'],d['IDD Code'],d['Currency'],d['Language'],d['City Lat/Lon'],
        d['Country Lat/Lon'],d['Continent Lat/Lon'],d['Sunrise / Sunset'],
        d['Timezone GMT offset'],d['Local Time'],d['Time Zone']]

    print R+'\n[IP INFO]'+ENDC,info
    return info
                  