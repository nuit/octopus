#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# OCT0PUS Botnet - by: dtty
import re, socket, os
import time
import random
import asynchat
import asyncore
import threading

##################[ configs ]##################
bufsize    = 8096
channel    = "#"
uname      = "octopus"
realname   = "octopus"
botnick    = "octopus"
# BRIDGE SERVER
bhost      = 'localhost'
bport      = 2666
# IRC SERVER
ircserver  = "irc.freenode.net"
ircsname   = ircserver.split('.')[1].title()
ircport    = 6667     
###############################################

####[ colors ]###
R='\033[91m'
G='\033[92m'
B='\033[94m'
ENDC='\033[0m'
#################
 
 
class ChatClient(asynchat.async_chat): 
    def __init__(self, host, port):
        asynchat.async_chat.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect((host, port))
        self.set_terminator('\n')
        self.buffer = []
 
    def collect_incoming_data(self, data):
        pass
 
    def found_terminator(self):
        pass
      
def ping():
    global ircsock
    ircsock.send ("PONG :pingis\n")            

def sendmsg (chan, msg):
    global ircsock
    ircsock.send ("PRIVMSG "+ str(chan) +" :"+ str(msg) + "\n")

def JoinChan (chan):
    global ircsock
    print G+'\n[+] Joining channel:'+ENDC,channel,'...'
    ircsock.send ("JOIN "+ chan +"\n")

def ConnectIRC(server,port):
    global ircsock
    ircsock = socket.socket (socket.AF_INET, socket.SOCK_STREAM)                                
    ircsock.connect ((server, port))
    print G+'\n[+] Connected on:'+ENDC,str(server)+':'+str(port)
    ircsock.send ("USER " + uname + " 2 3 " + realname + "\n")
    ircsock.send ("NICK "+ botnick + "\n")

    JoinChan (channel)     

    emoticon=[':D',':)',':P',';P','xP','xD',':O',':3',':/','o_0','0_o','^^','😈','😈😈😈','😈😈',
              '(◣_◢)','(≧ω≦)','(⋋▂⋌)','(✖╭╮✖)','o(≧o≦)o','😣','😱','😱😱😱','ლ(╹◡╹(ლ', '(~￣▽￣)~', 
              '(◕^^◕)', '(｡◕‿◕｡)','{◕ ◡ ◕}','✖‿✖','ʘ‿ʘ', '(◕‿◕✿)','(≧◡≦)','(◑‿◐)','(◕‿-)','(╯︵╰,)',
              '(︶ω︶)','(+_+)','(u_u)','(︶︹︺)', '(॓_॔)', '😣', '😱','😱😱😱','(º_º)','(⊙_◎)', 
              '(ñ_ñ)','(¬‿¬)','(∪ ◡ ∪)','(─‿‿─)','¯\_(ツ)_/¯','(¬▂¬)','(∩▂∩)', '(¬_¬)(｡♥‿♥｡)',
              '(✿ ♥‿♥)','♥╣[-_-]╠♥'] 
    sendmsg (channel, random.choice(emoticon))

    client = ChatClient(bhost, bport)
    comm = threading.Thread(target=asyncore.loop)
    comm.daemon = True
    comm.start()
    
    while True:              
        ircmsg = ircsock.recv (bufsize)
        ircmsg = ircmsg.strip ('\n\r')
        print B+'['+ircsname+']'+ENDC,ircmsg        

        if ircmsg.find ("PING :") != -1:
            ping()

        if ircmsg.find ("PRIVMSG") != -1 and ircmsg.find (channel) != -1:                        
            if server not in ircmsg:
                if ':['+ircsname+']' not in ircmsg:
                    privmsg=ircmsg.split()[3::]
                    print 'privmsg:',privmsg
                    privmsg=' '.join(privmsg)
                    privmsg=privmsg[1::] # remove ":"
                    iuser=ircmsg.split()[0]
                    ircuser = iuser[iuser.find(':')+len(':'):iuser.rfind('!')]                    
                    msg='a1x['+ircsname+'] <'+str(ircuser)+'>'+' '+str(privmsg)
                    print B+"\n[+] Sending PRIVMSG:"+ENDC,msg
                    client.push(str(msg) + '\n')

        if ircmsg.find ("ERROR :Closing Link:") != -1:                                                                          
            ConnectIRC(server=ircserver,port=ircport)   

        if ircmsg.find ("KICK") != -1:                       
            print G+'\n[+] Rejoining:'+ENDC,channel,'...\n'
            ircsock.send ("JOIN "+ channel + "\n")                      

ConnectIRC(server=ircserver,port=ircport)        
