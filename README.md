# OCT0PUS

Python IRC Bot

### Requirements:

$ sudo apt-get install python-pip python-dev build-essential 

$ pip install virtualenv

$ cd octopus && virtualenv -p /usr/bin/python2.7 venv

$ cd venv/ && source bin/activate

$ pip install -r requirements.txt


### Install:

$ git clone https://gitlab.com/nuit/octopus.git

$ cd octopus

$ python fserver.py && python flistener.py && python eclient.py

$ python eserver.py && python elistener.py && python fclient.py


## License
Copyleft (ɔ)


## Acknowledgments
Haze Crew
